//
//  ContentView.swift
//  webrtcexp
//
//  Created by Patryk Rogalski on 29/11/2023.
//

import SwiftUI
import WebKit
import WebRTC
import zdkpb
import ZDK

struct ContentView: View {
    
    @State private var showingErrorAlert = false
    @State private var errorMessage = ""
    
    @State private var roomId: String = "09910592-8cd5-456b-9868-eba41c3b8aca"
    @State var clientId: String?
    @State var clientNickname: String?
    @State var userStatus: UserStatus = .inactive
    @State var roomStatus: RoomStatus = .idle
    @State var roomCapacity: Int = 0
    @State var roomMembersCount: Int = 0
    @State var conferenceStatus: ConferenceStatus = .idle
    @State var wsStatus: TransportStatus = .unknown
    @State var chatsIds: [String] = []
    @State var senderStatus: RTCPeerConnectionState = .disconnected
    @State var receiverStatus: RTCPeerConnectionState = .disconnected
    @State var localVideoTrack: RTCVideoTrack? = nil
    @State var localAudioTrack: RTCAudioTrack? = nil
    @State var isVideoMuted: Bool = false
    @State var membersWithMedia: [MemberWithMedia] = []
    @State var trackInfo: [String:TrackInfo] = [:]
    @State var audios: String = ""
    @State var roomChatId: String? = nil
    
    @State var showDeviceModuleSettings: Bool = false
    
    @State var showingUserSection = true
    @State var showingWebSocketSection = true
    @State var showingRoomSection = true
    @State var showingChatSection = false
    @State var showingRoomChatSection = false
    @State var showingDeviceSection = true
    
    var body: some View {
        NavigationStack {
            List {
                Button("activate demo") {
                    Task<Void, Never>{
                        do {
                            try await activateDemo()
                        } catch {
                            printError("failed to activate demo: \(error)")
                        }
                    }
                }
                
                Section(isExpanded: $showingUserSection, content: {
                    Text("MODULE STATUS: \(userStatus.rawValue)")
                        .onReceive(user.data.$status.receive(on: DispatchQueue.main)){ x in self.userStatus = x}
                    Text("ID: \(clientId ?? "<unknown>")")
                        .onReceive(user.data.$clientId.receive(on: DispatchQueue.main)){ x in self.clientId = x}
                    Text("Nickname: \(clientNickname ?? "<unknown>")")
                        .onReceive(user.data.$clientNickname.receive(on: DispatchQueue.main)){ x in self.clientNickname = x}
                }, header: {
                    Text("User Module")
                })
                
                Section(isExpanded: $showingWebSocketSection, content: {
                    Text("MODULE STATUS: \(wsStatus.rawValue)")
                        .onReceive(websocket.data.$status.receive(on: DispatchQueue.main)){ x in self.wsStatus = x}
                }, header: {
                    Text("WebSocket Module")
                })
                
                Section(isExpanded: $showingRoomSection,content: {
                    Text("MODULE STATUS: \(roomStatus.rawValue)")
                        .onReceive(room.data.$status.receive(on: DispatchQueue.main)){ x in self.roomStatus = x}
                    Text("Capacity: \(roomCapacity)")
                        .onReceive(room.data.$capacity.receive(on: DispatchQueue.main)){ x in self.roomCapacity = x}
                    Text("Members Count: \(roomMembersCount)")
                        .onReceive(room.data.$members.receive(on: DispatchQueue.main)){ x in self.roomMembersCount = x.count}
                    TextField("Enter roomId", text: $roomId)
                        .disableAutocorrection(true)
                        .border(.secondary)
                    Button("join") {
                        Task<Void, Never> {
                            do {
                                try await room.actions.joinRoom(roomId: self.roomId)
                            } catch {
                                printError("failed to join to room: \(error)")
                            }
                        }
                    }.disabled((roomStatus == .ready || roomStatus == .reloading))
                    Button("leave", role: .cancel) {
                        Task<Void, Never> {
                            do {
                                try await room.actions.leaveRoom()
                            } catch {
                                printError("failed to leave room: \(error)")
                            }
                        }
                    }.disabled(roomStatus != .ready)
                    NavigationLink(destination: BlockedMembersView()) {
                        Text("view blocked members")
                    }.disabled(roomStatus != .ready)
                }, header: {
                    Text("Room Module")
                })
                
                Section(isExpanded: $showingChatSection, content: {
                    Text("MODULE STATUS: \(chatsIds.count)")
                        .onReceive(chat.data.$chatsIds.receive(on: DispatchQueue.main)){ x in self.chatsIds = x}
                }, header: {
                    Text("Chat Module")
                })
                
                Section(isExpanded: $showingRoomChatSection, content: {
                    Text("MODULE STATUS: \((roomChatId != nil ? roomChatId! : ""))").onReceive(roomChat.data.chatId.receive(on: DispatchQueue.main)){ x in self.roomChatId = x}
                    NavigationLink(destination: RoomChatView()) {
                        Text("view room chat")
                    }.disabled(roomStatus != .ready)
                }, header: {
                    Text("Room Chat Module")
                })
                
                Section(isExpanded: $showingDeviceSection, content: {
                    localVideoTrack == nil ? Button("cam on"){
                        Task<Void, Never>{
                            print("click cam on")
                            do {
                                try await device.actions.turnOnCam(constraints: CameraConstraints(video: VideoConstraints(width: MinIdealMaxValue(ideal: 540), height: MinIdealMaxValue(ideal: 540), frameRate: MinIdealMaxValue(ideal: 30))))
                            }catch{
                                printError("failed to turn on the cam: \(error)")
                            }
                        }
                    } : Button("cam off"){
                        Task{
                            print("click cam off")
                            await device.actions.turnOffCam()
                        }
                    }
                    if localVideoTrack != nil {
                        Button("switch Cam") {
                            Task {
                                do {
                                    try await device.actions.switchCam()
                                } catch {
                                    printError("Failed to switch cam: \(error)")
                                    // Optionally, add more user-friendly error handling here
                                }
                            }
                        }
                    }
                    if localVideoTrack != nil {
                        !self.isVideoMuted ? Button("mute cam"){
                            Task{
                                await device.actions.muteCam()
                            }
                        } :  Button("unmute cam"){
                            Task {
                                await device.actions.unmuteCam()
                            }
                        }
                    }
                    localAudioTrack == nil ? Button("mic on"){
                        Task<Void, Never>{
                            do {
                                try await device.actions.turnOnMic()
                            }catch{
                                printError("failed to turn on the mic: \(error)")
                            }
                        }
                    } : Button("mic off"){
                        Task {
                            await device.actions.turnOffMic()
                        }
                    }
                    
//                    HStack{
//                        camButtons
//                        micButtons
//                    }
                    Button("open device settings") {
                        self.showDeviceModuleSettings.toggle()
                    }
                    .sheet(isPresented: self.$showDeviceModuleSettings) {
                        DeviceSettings()
                    }
                }, header: {
                    Text("Device Module")
                })
                
                Section(header: Text("Conference Module")) {
                    Text("MODULE STATUS: \(conferenceStatus.rawValue)")
                        .onReceive(conference.data.$status.receive(on: DispatchQueue.main)){ x in self.conferenceStatus = x}
                    Text("Sender status: \(senderStatus.description)")
                        .onReceive(conference.data.senderConnectionState.receive(on: DispatchQueue.main)){ x in self.senderStatus = x}
                    Text("Receiver status: \(receiverStatus.description)")
                        .onReceive(conference.data.receiverConnectionState.receive(on: DispatchQueue.main)){ x in self.receiverStatus = x}
                    ForEach(membersWithMedia) { member in
                        MemberView(member: member) { action in
                            switch action {
                            case .block:
                                Task<Void, Never>{
                                    do {
                                        try await room.actions.blockMember(userId: member.member.userID, duration: 10, reason: "")
                                    } catch {
                                        printError("failed to block member: \(error)")
                                    }
                                }
                                break
                            case .kick:
                                Task<Void, Never>{
                                    do {
                                        try await room.actions.kickMember(userId: member.member.userID, reason: "")
                                    } catch {
                                        printError("failed to kick member: \(error)")
                                    }
                                }
                                break
                            }
                        }
                    }
                    .onReceive(conference.data.$membersWithMedia.receive(on: DispatchQueue.main)){ x in self.membersWithMedia = x}
                }
            }
            .listStyle(.sidebar)
            .navigationTitle("ZDK Sample")
            .navigationBarTitleDisplayMode(.inline)
            .onReceive(device.data.$localVideoMuted.receive(on: DispatchQueue.main)){x in self.isVideoMuted = x}.alert(errorMessage, isPresented: $showingErrorAlert, actions: {
                Button("OK") {
                    showingErrorAlert = false
                }
            })
            .onReceive(device.data.$localAudioTrack.receive(on: DispatchQueue.main)){x in self.localAudioTrack = x}
            .onReceive(device.data.$localVideoTrack.receive(on: DispatchQueue.main)){x in self.localVideoTrack = x
                print("camera: \(x != nil ? true : false)")
            }
            .onReceive(user.data.$clientId.receive(on: DispatchQueue.main)){x in self.clientId = x}
        }
        
        
    }
    
    func activateDemo() async throws {
        let token = try await getToken()
        await device.actions.setSpeaker(speakerType: .speaker)
        let user = try await user.actions.open(token: token)
        print("Authenticated user: id: \(user.id), nickname: \(user.nickname)")
        try await websocket.actions.open()
        try await room.actions.joinRoom(roomId: self.roomId)
        //        try await chat.actions.joinChat(chatId: "0876250e-f3e1-4ba8-9754-bd43e6b6b80a")
        //print("joined room!")
    }
    
    func printError(_ msg: String) {
        print(msg)
        errorMessage = msg
        showingErrorAlert = true
    }
    
    
}

struct DeviceSettings: View {
    @State var selectedCamera: Camera? = nil
    @State var selectedMicrophone: Microphone? = nil
    @State var selectedSpeaker: SpeakerType = device.data.selectedSpeaker
    @State var availableCameras: [Camera] = []
    @State var availableMicrophones: [Microphone] = []
    @State var availableSpeakers: [SpeakerType] = [.earSpeaker, .speaker]
    
    var body: some View {
        NavigationStack{
            VStack(alignment: .leading){
                Form {
                    Picker(selection: self.$selectedMicrophone, label: Text("Microphone")){
                        ForEach(self.availableMicrophones, id: \.self) { (item) in
                            Text(item.name).tag(Optional(item))
                        }
                    }
                    .onChange(of: self.selectedMicrophone) { oldValue, newValue in
                        Task {
                            try await device.actions.setMicrophone(microphoneId: newValue!.id)
                        }
                    }
                    Picker(selection: self.$selectedCamera, label: Text("Camera")){
                        ForEach(self.availableCameras, id: \.self) { (item) in
                            Text(item.name).tag(Optional(item))
                        }
                    }
                    .onChange(of: self.selectedCamera) { oldValue, newValue in
                        Task {
                            try await device.actions.setCamera(cameraId: newValue?.id)
                        }
                    }
                    Picker(selection: self.$selectedSpeaker, label: Text("Speaker")){
                        ForEach(self.availableSpeakers, id: \.self) { (item) in
                            Text(item.rawValue).tag(item)
                        }
                    }
                    .onChange(of: self.selectedSpeaker) { oldValue, newValue in
                        Task {
                            await device.actions.setSpeaker(speakerType: newValue)
                        }
                    }
                }
            }
            .navigationTitle("Device Module Settings")
        }
        .presentationDetents([.height(200)])
        .onReceive(device.data.$availableCameras.receive(on: DispatchQueue.main)){x in self.availableCameras = x}
        .onReceive(device.data.$selectedCamera.receive(on: DispatchQueue.main)){x in self.selectedCamera = x}
        .onReceive(device.data.$availableMicrophones.receive(on: DispatchQueue.main)){x in self.availableMicrophones = x}
        .onReceive(device.data.$selectedMicrophone.receive(on: DispatchQueue.main)){x in self.selectedMicrophone = x }
        .onReceive(device.data.$selectedSpeaker.receive(on: DispatchQueue.main)){x in self.selectedSpeaker = x}
    }
        
}
