//
//  ChatView.swift
//  webrtcexp
//
//  Created by Sylwia Fluder on 15/02/2024.
//

import Foundation
import SwiftUI
import Combine
import ZDK

struct ChatListView: View {
    
    @State private var chatMemberId: String = ""
    @State private var chatId: String = "0876250e-f3e1-4ba8-9754-bd43e6b6b80a"
    @State var userStatus: UserStatus = .inactive
    @State var wsStatus: TransportStatus = .unknown
    @State var activeChatsIds: [String] = []
    
    var body: some View {
        VStack {
            Button("activate chat demo") {
                Task<Void, Never>{
                    do {
//                        try await activateDemo()
                    } catch {
                        print("failed to activate demo: \(error)")
                    }
                }
            }
            Text("---- User ----")
            Text("MODULE STATUS: \(userStatus.rawValue)").onReceive(user.data.$status.receive(on: DispatchQueue.main)){ x in self.userStatus = x}
            Text("---- WebSocket ----")
            Text("MODULE STATUS: \(wsStatus.rawValue)").onReceive(websocket.data.$status.receive(on: DispatchQueue.main)){ x in self.wsStatus = x}
            Button("join chat") {
                Task<Void, Never>{
                    do {
                        try await chat.actions.joinChat(chatId: chatId)
                    } catch {
                        print("failed to joined chat: \(error)")
                    }
                }
            }
            Button("leave chat") {
                Task<Void, Never>{
                    do {
                        try await chat.actions.leaveChat(chatId: chatId)
                    } catch {
                        print("failed to leave chat: \(error)")
                    }
                }
            }
            Button("send message") {
                Task<Void, Never>{
                    do {
                        try await chat.actions.sendMessage(chatId: chatId, content: DateFormatter.localizedString(from: Date(), dateStyle: .long, timeStyle: .medium))
                    } catch {
                        print("failed to send message: \(error)")
                    }
                }
            }
            Button("block chat member") {
                Task<Void, Never>{
                    do {
                        try await chat.actions.blockChatMember(chatId: chatId, chatMemberId: chatMemberId, duration: 5, reason: nil)
                    } catch {
                        print("failed to block chat member: \(error)")
                    }
                }
            }
            Button("unblock chat member") {
                Task<Void, Never>{
                    do {
                        try await chat.actions.unblockChatMember(chatId: chatId, chatMemberId: chatMemberId)
                    } catch {
                        print("failed to unblock chat member: \(error)")
                    }
                }
            }
            Button("mute chat member") {
                Task<Void, Never>{
                    do {
                        try await chat.actions.muteChatMember(chatId: chatId, chatMemberId: chatMemberId, duration: 5, reason: nil)
                    } catch {
                        print("failed to mute chat member: \(error)")
                    }
                }
            }
            Button("unmute chat member") {
                Task<Void, Never>{
                    do {
                        try await chat.actions.unmuteChatMember(chatId: chatId, chatMemberId: chatMemberId)
                    } catch {
                        print("failed to unmute chat member: \(error)")
                    }
                }
            }
            Button("kick chat member") {
                Task<Void, Never>{
                    do {
                        try await chat.actions.kickChatMember(chatId: chatId, chatMemberId: chatMemberId,  reason: nil)
                    } catch {
                        print("failed to unmute chat member: \(error)")
                    }
                }
            }
            TextField("Enter chatMemberId", text: $chatMemberId)
                .disableAutocorrection(true)
                .border(.secondary)
                .padding()
            TextField("Enter chatId", text: $chatId)
                .disableAutocorrection(true)
                .border(.secondary)
                .padding()
            Text("---- Chats ----")
            List(self.activeChatsIds, id: \.self) { chatId in
                ChatView(chatId: chatId)
            }
        }.onReceive(chat.data.$chatsIds.receive(on: DispatchQueue.main)){x in self.activeChatsIds = x}
    }
        
        func activateDemo() async throws {
            let token = try await getToken()
            try await user.actions.open(token: token)
            try await websocket.actions.open()
            print("joined chat!")
        }
}

struct ChatView: View {
    var chatId: String
    
    @State var chatInfo: ChatInfo? = nil
    @State var chatStatus: ChatStatus? = nil
    @State var chatMessages: [ChatMessage] = []


    var body: some View {
       
        VStack(alignment: .leading, spacing: 8) {
            if let chatInfo = chatInfo {
                    VStack(alignment: .center) {
                        Text("Chat ID: \(chatInfo.id)").font(.headline)
                        Text("Chat status: \(chatStatus?.rawValue ?? "Unknown")")
                            .font(.headline).onReceive(chat.data.getChatStatus(id: chatId)){x in self.chatStatus = x}
                        Text("Messages: \(chatMessages.count)").font(.subheadline)
                        VStack {
                            ForEach(chatMessages, id: \.id) { message in
                                MessageView(message: message)
                            }
                        }.onReceive(chat.data.getChatMessages(id: chatId)) { messages in
                            self.chatMessages = messages
                        }
                    }
                } else {
                    ProgressView()
                }
        }.padding().onReceive(chat.data.getChat(id: chatId).receive(on: DispatchQueue.main)){x in self.chatInfo = x}
    }
}

struct MessageView: View {
    let message: ChatMessage

    var body: some View {
        HStack {
            Text(message.content)
            Spacer()
            Button(action: {
                Task {
                    do {
                        let updatedContent = DateFormatter.localizedString(from: Date.now, dateStyle: .long, timeStyle: .medium)
                        try await chat.actions.updateMessage(chatId: message.chatId, messageId: message.id, content: updatedContent)
                    } catch {
                        print("Failed to update message: \(error)")
                    }
                }
            }) {
                Text("Update Message")
            }
            .buttonStyle(BorderlessButtonStyle())
            Button(action: {
                Task {
                    do {
                        try await chat.actions.deleteMessage(chatId: message.chatId, messageId: message.id)
                    } catch {
                        print("Failed to delete message: \(error)")
                    }
                }
            }) {
                Text("Delete Message")
            }
            .buttonStyle(BorderlessButtonStyle())
            if message.sendingStatus == .interrupted {
                Button(action: {
                    Task {
                        do {
                            try await chat.actions.resendMessage(chatId: message.chatId, messageId: message.id)
                        } catch {
                            print("Failed to resend message: \(error)")
                        }
                    }
                }) {
                    Text("Resend Message")
                }
                .buttonStyle(BorderlessButtonStyle())
            }
        }.background(backgroundColor(for: message))
    }
}

func backgroundColor(for message: ChatMessage) -> Color {
    if message.sendingStatus == .loading || message.updateStatus == .loading || message.deleteStatus == .loading {
        return Color.blue.opacity(0.2)
    }
    else if message.sendingStatus == .interrupted || message.updateStatus == .interrupted || message.deleteStatus == .interrupted {
        return Color.red.opacity(0.2)
    }
    else {
        return Color.green.opacity(0)
    }
}
