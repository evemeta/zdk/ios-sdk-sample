//
//  ZDK_SampleApp.swift
//  ZDK Sample
//
//  Created by Patryk Rogalski on 03/04/2024.
//

import SwiftUI
import ZDK

let zdk: ZDK = ZDK(host: "dev.zu.casa")
let user: UserModule = zdk.user
let websocket: WebSocketModule = zdk.websocket
let room: RoomModule = zdk.room
let device: DeviceModule = zdk.device
let conference: ConferenceModule = zdk.conference
let chat: ChatModule = zdk.chat
let roomChat: RoomChatModule = zdk.roomChat

@main
struct ZDK_SampleApp: App {
    @UIApplicationDelegateAdaptor(AppDelegate.self) var appDelegate

    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}

struct TokenResult: Codable {
    let token: String
}

func getToken() async throws -> String {
    guard let tokenUrl = URL(string: "https://demo.dev.zu.casa/api/token") else {
        fatalError("invalid url")
    }

    let (data, _) = try await URLSession.shared.data(from: tokenUrl)
    let tokenResult = try JSONDecoder().decode(TokenResult.self, from: data)
    return tokenResult.token
}
