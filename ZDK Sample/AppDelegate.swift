//
//  AppDelegate.swift
//  webrtcexp
//
//  Created by Patryk Rogalski on 26/02/2024.
//

import Foundation
import UIKit

class AppDelegate: NSObject, UIApplicationDelegate {
    static private(set) var instance: AppDelegate! = nil
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
        AppDelegate.instance = self
        return true
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        print("#app is about to die!")
        if room.data.status != .idle {
            conference.destroy()
            room.actions.leaveRoom { _ in }
            Task{
                try await Task.sleep(nanoseconds: UInt64(3 * Double(NSEC_PER_SEC)))
                print("done!")
            }
        }
    }
}
