//
//  BlockedMembersView.swift
//  webrtcexp
//
//  Created by Patryk Rogalski on 21/02/2024.
//

import Foundation
import SwiftUI

struct BlockedMembersView: View {
    @State var blockedMembersIds: [String] = []
    
    var body: some View {
        blockedMembers
            .navigationTitle("Blocked Member Ids")
    }
    
    var blockedMembers: some View {
        HStack{
            List {
                ForEach(blockedMembersIds, id: \.self) { memberId in
                    HStack{
                        Button("unblock \(memberId)") {
                            Task<Void, Never>{
                                do {
                                    try await room.actions.unblockMember(userId: memberId)
                                } catch {
                                    print("failed to unblock member: \(error)")
                                }
                            }
                        }
                    }
                }
            }
        }.onReceive(room.data.$blockedMembersIds.receive(on: DispatchQueue.main)){ x in self.blockedMembersIds = x}
    }
}

#Preview {
    BlockedMembersView()
}
