//
//  MemberView.swift
//  webrtcexp
//
//  Created by Patryk Rogalski on 02/04/2024.
//

import SwiftUI
import ZDK

enum MemberClickAction {
    case kick
    case block
}

struct MemberView: View {
    let member: MemberWithMedia
    let onClick: (MemberClickAction) -> Void
    
    @State private var speakerInfo: Speaker = Speaker.zero
    @State var showMemberSettings: Bool = false
    
    var body: some View {
        VStack(alignment: .leading) {
            HStack{
                ZStack{
                    switch member.video.status {
                    case .Loading:
                        ProgressView()
                    case .Ready:
                        VideoTrackView(track: member.video.track!, videoContentMode: .scaleAspectFit, mirror: user.data.clientId == member.member.userID)
                    default:
                        Image(systemName: "network.slash")
                            .imageScale(.large)
                            .foregroundStyle(.tint)
                    }
                    
                    if speakerInfo.audioLevel >= conference.data.speakerAudioLevelThreshold  {
                        ZStack(alignment: .topLeading, content: {
                            Color.clear
                            Circle()
                                .fill(.green)
                                .frame(width: 15, height: 15)
                                .padding(EdgeInsets(top: 5, leading: 5, bottom: 0, trailing: 0))
                        })
                    }
                }.frame(width: 150)
                
                VStack(alignment: .leading){
                    Text("User ID: \(member.member.userID)")
                    Text("Video Status: \(member.video.status.rawValue)")
                    Text("Audio Status: \(member.audio.status.rawValue)")
                    Text("Audio Level: \(speakerInfo.audioLevel)")
                }
                
            }
            Button("settings") {
                self.showMemberSettings.toggle()
            }
            .sheet(isPresented: self.$showMemberSettings) {
                MemberSettings(memberId: member.member.userID, onClick: self.onClick)
            }
            .buttonStyle(PlainButtonStyle())
        }.onReceive(conference.data.$speakers.receive(on: DispatchQueue.main)){ value in
            self.speakerInfo = value.first { speaker in speaker.userId == member.member.userID } ?? Speaker.zero
        }
    }
}

struct MemberSettings: View {
    private let memberId: String
    private let onClick: (MemberClickAction) -> Void
    @State private var volume: Double = 1
    
    init(memberId: String, onClick: @escaping (MemberClickAction) -> Void) {
        self.memberId = memberId
        self.onClick = onClick
    }
    
//    @State var selectedCamera: Camera? = nil
//    @State var selectedMicrophone: Microphone? = nil
//    @State var selectedSpeaker: SpeakerType = device.data.selectedSpeaker
//    @State var availableCameras: [Camera] = []
//    @State var availableMicrophones: [Microphone] = []
//    @State var availableSpeakers: [SpeakerType] = [.earSpeaker, .speaker]
    
    var body: some View {
        NavigationStack{
            VStack(alignment: .leading){
                Form {
                    if user.data.clientId != self.memberId {
                        Section {
                            Slider(value: self.$volume, in: 0...1, step: 0.01)
                            .onChange(of: self.volume) { oldValue, newValue in
                                Task {
                                    do {
                                        print("volume set: \(newValue)")
                                        try await conference.actions.setUserVolume(userId: self.memberId, volume: newValue)
                                    } catch {
                                        print("Failed to set volume \(error)")
                                    }
                                }
                            }
                            Text("Volume: \(self.volume * 100)")
                        }
                        Section(header: Text("Moderation")) {
                            Button("Kick member") {
                                self.onClick(.kick)
                            }
                            Button("Block member") {
                                self.onClick(.block)
                            }
                        }
                    }
                    
//                    Picker(selection: self.$selectedMicrophone, label: Text("Microphone")){
//                        ForEach(self.availableMicrophones, id: \.self) { (item) in
//                            Text(item.name).tag(Optional(item))
//                        }
//                    }
//                    .onChange(of: self.selectedMicrophone) { oldValue, newValue in
//                        Task {
//                            try await device.actions.setMicrophone(microphoneId: newValue!.id)
//                        }
//                    }
//                    Picker(selection: self.$selectedCamera, label: Text("Camera")){
//                        ForEach(self.availableCameras, id: \.self) { (item) in
//                            Text(item.name).tag(Optional(item))
//                        }
//                    }
//                    .onChange(of: self.selectedCamera) { oldValue, newValue in
//                        Task {
//                            try await device.actions.setCamera(cameraId: newValue?.id)
//                        }
//                    }
//                    Picker(selection: self.$selectedSpeaker, label: Text("Speaker")){
//                        ForEach(self.availableSpeakers, id: \.self) { (item) in
//                            Text(item.rawValue).tag(item)
//                        }
//                    }
//                    .onChange(of: self.selectedSpeaker) { oldValue, newValue in
//                        Task {
//                            await device.actions.setSpeaker(speakerType: newValue)
//                        }
//                    }
                }
            }
            .navigationTitle("Member Settings")
        }
        .presentationDetents([.height(350)])
        .onAppear(perform: {
            Task {
                self.volume = try await conference.actions.getUserVolume(userId: self.memberId)
                print("volume init: \(self.volume)")
            }
        })
//        .onReceive(device.data.$availableCameras.receive(on: DispatchQueue.main)){x in self.availableCameras = x}
//        .onReceive(device.data.$selectedCamera.receive(on: DispatchQueue.main)){x in self.selectedCamera = x}
//        .onReceive(device.data.$availableMicrophones.receive(on: DispatchQueue.main)){x in self.availableMicrophones = x}
//        .onReceive(device.data.$selectedMicrophone.receive(on: DispatchQueue.main)){x in self.selectedMicrophone = x }
//        .onReceive(device.data.$selectedSpeaker.receive(on: DispatchQueue.main)){x in self.selectedSpeaker = x}
    }
        
}
