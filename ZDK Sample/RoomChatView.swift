//
//  RoomChatView.swift
//  webrtcexp
//
//  Created by Sylwia Fluder on 13/03/2024.
//

import Foundation
import SwiftUI
import ZDK

struct RoomChatView: View {
    @State var blockedMembersIds: [String] = []
    @State private var chatMemberId: String = ""
    @State var chatInfo: ChatInfo? = nil
    @State var chatStatus: ChatStatus? = nil
    @State var chatMessages: [ChatMessage] = []
    
    
    var body: some View {
        TextField("Enter chatMemberId", text: $chatMemberId)
            .disableAutocorrection(true)
            .border(.secondary)
            .padding()
        Button("send message") {
            Task<Void, Never>{
                do {
                    try await roomChat.actions.sendMessage(content: DateFormatter.localizedString(from: Date(), dateStyle: .long, timeStyle: .medium))
                } catch {
                    print("failed to send message: \(error)")
                }
            }
        }
        Button("block chat member") {
            Task<Void, Never>{
                do {
                    try await roomChat.actions.blockChatMember(chatMemberId: chatMemberId, duration: 5, reason: nil)
                } catch {
                    print("failed to block chat member: \(error)")
                }
            }
        }
        Button("unblock chat member") {
            Task<Void, Never>{
                do {
                    try await roomChat.actions.unblockChatMember(chatMemberId: chatMemberId)
                } catch {
                    print("failed to unblock chat member: \(error)")
                }
            }
        }
        Button("mute chat member") {
            Task<Void, Never>{
                do {
                    try await roomChat.actions.muteChatMember(chatMemberId: chatMemberId, duration: 5, reason: nil)
                } catch {
                    print("failed to mute chat member: \(error)")
                }
            }
        }
        Button("unmute chat member") {
            Task<Void, Never>{
                do {
                    try await roomChat.actions.unmuteChatMember(chatMemberId: chatMemberId)
                } catch {
                    print("failed to unmute chat member: \(error)")
                }
            }
        }
        Button("kick chat member") {
            Task<Void, Never>{
                do {
                    try await roomChat.actions.kickChatMember(chatMemberId: chatMemberId,  reason: nil)
                } catch {
                    print("failed to unmute chat member: \(error)")
                }
            }
        }
        chat
        
    }
    
    var chat: some View {
        VStack(alignment: .leading, spacing: 8) {
            if chatInfo != nil {
                VStack(alignment: .center) {
                    Text("Chat status: \(chatStatus?.rawValue ?? "Unknown")")
                        .font(.headline).onReceive(roomChat.data.getChatStatus()){x in self.chatStatus = x}
                    Text("Messages: \(chatMessages.count)").font(.subheadline)
                    VStack {
                        ForEach(chatMessages, id: \.id) { message in
                            MessageView(message: message)
                        }
                    }.onReceive(roomChat.data.getChatMessages()) { messages in
                        self.chatMessages = messages
                    }
                }
            } else {
                ProgressView()
            }
        }.padding().onReceive(roomChat.data.getChat().receive(on: DispatchQueue.main)){x in self.chatInfo = x;
            print("CHAT INFP UPDATED")
            print(x)
        }
}
}

#Preview {
    RoomChatView()
}
